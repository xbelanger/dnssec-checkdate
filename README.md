# dnssec-checkdate script

This script displays the start and end dates for a SOA RRSIG record, if available.

You must provide a domain name to validate as an argument to the script.

