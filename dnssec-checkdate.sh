#!/bin/bash

# Xavier Belanger
# November 22, 2024

if [ $# -ne 1 ]
then
	/bin/printf "You need to provide a domain name as an argument.\n"
	/bin/printf "Example: $0 example.net\n"
	exit 1
fi

domain=$(echo "$1")
rrsigFromSoaRecord=$(/usr/bin/dig +dnssec SOA $domain | /bin/grep ^$domain.*RRSIG[[:space:]]SOA)

if [ $? -ne 0 ]
then
	/bin/printf "SOA RRSIG not found or non-existent domain.\n"
	exit 1
else

	startDate=$(echo $rrsigFromSoaRecord | /bin/cut -d" " -f10)
	endDate=$(echo $rrsigFromSoaRecord  | /bin/cut -d" " -f9)

	# converting date strings with sed
	# from input YYYYMMDDHHMMSS
	# to "YYYY Month DD HH:MM:SS"
	/bin/printf "Start date:\t"
	startDate=$(echo $startDate | /bin/sed 's/^\(.\{8\}\)\(.\{2\}\)\(.\{2\}\)\(.\{2\}\)/\1 \2:\3:\4/')
	/bin/date --date "$(echo $startDate)" +"%Y %B %d %H:%M:%S"

	/bin/printf "End date:\t"
	endDate=$(echo $endDate | /bin/sed 's/^\(.\{8\}\)\(.\{2\}\)\(.\{2\}\)\(.\{2\}\)/\1 \2:\3:\4/')
	/bin/date --date "$(echo $endDate)" +"%Y %B %d %H:%M:%S"
fi

# EoF
